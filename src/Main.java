import java.io.*;
import java.util.Scanner;

public class Main {
    public static Game game;

    public static void main(String[] args) {
        final int RESUME_GAME = 1;
        final int NEW_GAME = 2;
        final int EXIT = 3;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Tic Tac Toe");
        printMenu();
        int choice = Integer.parseInt(scanner.nextLine());

        while (choice != EXIT) {
            switch (choice) {
                case RESUME_GAME:
                    System.out.println("Pls enter the name of the game");
                    resumeGame(scanner.nextLine());
                    break;
                case NEW_GAME:
                    System.out.println("you can save your game by pressing space and entering name for the save");
                    newGame();

                    break;
                case EXIT:
                    break;
                default:
                    System.out.println("You entered invalid input");
            }
            if (choice != EXIT) {
                printMenu();
                choice = Integer.parseInt(scanner.nextLine());
            }
        }

    }

    public static boolean saveGame() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter name for the game");
        game.setName(scanner.nextLine());
        String FILE_NAME = "games.dat";
        File file = new File(FILE_NAME);
        try (FileOutputStream fileOutput = new FileOutputStream(file, true);
             ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput)) {
            objectOutput.writeObject(game);
            System.out.println("game was saved");
            return true;
        } catch (IOException e) {
            System.out.println("Error writing into " + FILE_NAME);
        }
        return false;
    }

    public static void resumeGame(String gameName) {
        String GAME_NAME = "games.dat";
        game = null;
        try (FileInputStream fileInput = new FileInputStream(GAME_NAME)) {
            while (fileInput.available() > 0) {
                ObjectInputStream objectInput = new ObjectInputStream(fileInput);
                game = (Game) objectInput.readObject();
                if ((game.getName().equals(gameName))) {
                    startGame();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(GAME_NAME + " could not be found!");
        } catch (IOException e) {
            System.out.println("Error reading from " + GAME_NAME);
        } catch (ClassNotFoundException e) {
            System.out.println("class 'Game' was not found!");
        } finally {
            System.out.println("this game doesn't exists");
        }

    }

    public static void newGame() {
        game = new Game();
        startGame();
    }

    public static void startGame() {
        Scanner scanner = new Scanner(System.in);
        String row;
        String col;
        boolean isGameEnded = isGameEnded(game);

        while (game.getTurn() < 9 && !isGameEnded) {
            System.out.println("Enter the row where you want to put your sign");
            row = scanner.nextLine();
            System.out.println("Enter the colon where you want to put your sign");
            col = scanner.nextLine();
            if ((row.equals(" ") || col.equals(" "))) {
               isGameEnded = saveGame();
            } else {
                if(' ' == (game.getMap()[(Integer.parseInt(row) - 1) * 2][(Integer.parseInt(col) - 1) * 2].getSign())) {
                    game.getMap()[(Integer.parseInt(row) - 1) * 2][(Integer.parseInt(col) - 1) * 2].setSign(game.getTurn());
                    game.printMap();
                    game.increaseTurn();
                } else {
                    System.out.println("this cube is already full");
                }
            }
            isGameEnded = isGameEnded(game);
        }
    }

    public static boolean isGameEnded(Game game) {
        if (game.getTurn() < 5) {
            return false;
        }
        char lastSign = ' ';
        for (int raw = 0; raw < 3; raw++) {
            for (int cal = 0; cal < 3; cal++) {
                if (cal == 0) {
                    lastSign = game.getSign(raw, cal);
                } else if (game.getSign(raw, cal) == lastSign) {

                    if (cal == 2) {
                        return true;
                    }
                }
            }
        }
        for (int cal = 0; cal < 3; cal++) {
            for (int raw = 0; raw < 3; raw++) {
                if (raw == 0) {
                    lastSign = game.getSign(raw, cal);
                } else if (game.getSign(raw, cal) == lastSign) {
                    if (raw == 2)
                        return true;
                    else
                        raw = 3;
                }
            }
        }

        for (int index = 0; index < 3; index++) {
            if (index == 0)
                lastSign = game.getSign(index, index);
            else if (game.getMap()[index][index].getSign() == lastSign)
                if (index == 2) {
                    return true;
                } else {
                    index = 3;
                }
        }

        for (int index = 2; index >= 0; index--) {
            if (index == 2)
                lastSign = game.getSign(index, 0);
            else if (game.getSign(1, 1) == lastSign)
                if (index == 0)
                    return true;
                else {
                    index = -1;
                }
        }
        return false;
    }

    public static void printMenu() {
        final int RESUME_GAME = 1;
        final int NEW_GAME = 2;
        final int EXIT = 3;
        System.out.println();
        System.out.println("If you want to resume the game the pres: " + RESUME_GAME);
        System.out.println("If you want to start new game then pres: " + NEW_GAME);
        System.out.println("If you want to exit from the game the pres: " + EXIT);
        System.out.println("pls enter your choice");
    }
}
