import java.io.Serializable;

public class Game implements Serializable {
    public static final long serialVersionUID = 1;
    private Cube[][] map;
    private int turn;
    private final int MAP_SIZE = 5;
    private String name;

    public Game() {
        this.map = new Cube[MAP_SIZE][MAP_SIZE];
        for (int row = 0;row < MAP_SIZE; row++) {
            for (int col = 0;col < MAP_SIZE; col++) {
                if(row % 2 == 0) {
                    if(col % 2 == 0) {
                        map[row][col] = new Cube(' ');
                    } else {
                        map[row][col] = new Cube('|');
                    }
                } else {
                    map[row][col] = new Cube('-');
                }
            }
        }
        this.turn = 0;
        name = "";
    }

    public void printMap() {
        for (Cube[] row:map) {
            for(Cube cube: row) {
                System.out.print(cube.getSign());
            }
            System.out.println();
        }
    }

    public int getTurn() {
        return turn;
    }

    public Cube[][] getMap() {
        return map;
    }

    public char getSign(int raw, int cal) {
        return map[raw][cal].getSign();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void increaseTurn() {
        this.turn++;
    }
}
