import java.io.Serializable;

public class Cube implements Serializable {
    public static final long serialVersionUID = 1;
    private int row;
    private int col;
    private char sign;

    public Cube(char sign) {
        this.sign = sign;
    }

    public void setSign(int turn){
        if(turn % 2 ==0){
            this.sign = 'X';
        } else {
            this.sign = 'O';
        }
    }

    public char getSign() {
        return sign;
    }
}
